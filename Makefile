CC = g++
OBJECTS = oldmain.o calc.o
LIBRARIES = -lncurses
EXECUTABLE = sts

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) -o $@ $^ $(LIBRARIES)

%.o: %.cpp
	$(CC) -c $<

clean:
	rm -f $(OBJECTS) $(EXECUTABLE)
